// import CerrarSvg from 'src/assets/images/cerrar.svg'

export default {
    monthNames: [
      'ENERO',
      'FEBRERO',
      'MARZO',
      'ABRIL',
      'MAYO',
      'JUNIO',
      'JULIO',
      'AGOSTO',
      'SEPTIEMBRE',
      'OCTUBRE',
      'NOVIEMBRE',
      'DICIEMBRE'
    ],
    /**
     * @description Abre un nuevo enlace, reemplaza el uso de window.location.href, funcionalidad que tiene conflictos con el analisis de kiuwan
     * @param {string} href
     */
    openInTab(href) {
      Object.assign(document.createElement('a'), {
        href
      }).click()
    },

    /**
     * @description Abre un nuevo enlace, reemplaza el uso de window.location.href, funcionalidad que tiene conflictos con el analisis de kiuwan
     * @param {string} href
     */
    openInNewTab(href) {
      Object.assign(document.createElement('a'), {
        target: '_blank',
        href
      }).click()
    },

    /**
     * @description Entrega rut con puntos y guión
     * @param {string} rut
     * @return {string} rutPuntos
     */
    formatRut(rut) {
      const actual = rut.replace(/^0+/, '')
      let rutPuntos = ''
      if (actual != '' && actual.length > 1) {
        const sinPuntos = actual.replace(/\./g, '')
        const actualLimpio = sinPuntos.replace(/-/g, '')
        const inicio = actualLimpio.substring(0, actualLimpio.length - 1)

        let i = 0
        let j = 1
        for (i = inicio.length - 1; i >= 0; i--) {
          const letra = inicio.charAt(i)
          rutPuntos = letra + rutPuntos
          if (j % 3 == 0 && j <= inicio.length - 1) {
            rutPuntos = '.' + rutPuntos
          }
          j++
        }
        const dv = actualLimpio.substring(actualLimpio.length - 1)
        rutPuntos = rutPuntos + '-' + dv
      }
      return rutPuntos
    },

    /**
     * @description Funcion que permite evitar el paso de scripts en localstorage a modo de seguridad
     * @param {string} value
     * @return {string}
     */
    sanitizeValue(value) {
      try {
        return value.replace('<script>', '')
      } catch (e) {
        return 'null'
      }
    },

    /**
     * @description Funcion que permite buscar el valor asociado al nombre de un parametro encontrado en el URL
     * @param {object} name
     * @return {string}
     */
    getParameterByName(name) {
      const url = window.location.href
      name = name.replace(/[[]]/g, '$&')
      const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
      const results = regex.exec(url)
      if (!results) {
        return null
      }
      if (!results[2]) {
        return ''
      }
      return decodeURIComponent(results[2].replace('/+/g', ' '))
    },

    /**
     * @description Esta funcion retorna los datos de usuario almacenados en local que son asignados cuando el usuario inicia sesión
     * @return {object}
     */
    getUserData() {
      try {
        const userData = JSON.parse(localStorage.getItem('userData'))
        return userData
      } catch (e) {
        console.log('No hay datos de usuario almacenados de forma local')
        return null
      }
    },

    /**
     * @description
     * @return {object}
     */
    getMenu() {
      try {
        const menuData = JSON.parse(localStorage.getItem('menu'))
        return menuData
      } catch (e) {
        console.log('No hay datos de menú almacenados de forma local')
        return null
      }
    },

    /** @param {string} value
     * @return {string}
     */
    dateFormat(value) {
      const date = new Date(value)
      const day = ('0' + date.getDate()).slice(-2)
      const month = ('0' + (date.getMonth() + 1)).slice(-2)
      const year = date.getFullYear()
      return `${day}/${month}/${year}`
    },

    /** @param {number} input
     * @return {string}
     */
    generateDots(input) {
      input = input == undefined ? 0 : input
      const nums = input.toLocaleString('de-DE')
      return nums
    },

    /**
     * @return {string}
     * @description retorna la una cadena que concatena la fecha actual, esta función es útil para generar el nombre de la descarga de archivos
     */
    getTimeStampString() {
      const date = new Date()
      const day = date.getDate()
      const year = date.getFullYear()
      const hour = date.getHours()
      const minutes = date.getMinutes()
      const seconds = date.getSeconds()
      const month = date.getMonth() + 1
      const monthStr = month < 10 ? '0' + month : month + ''
      const dayStr = day < 10 ? '0' + day : day + ''
      return `${year}/${monthStr}/${dayStr} ${hour}:${minutes}:${seconds}`
    },

    /** @return {number} */
    getCleanRutEmpresa() {
      const user = Util.getUserData()
      const rut = parseInt(
        user['rutEmpresa'].substring(0, user['rutEmpresa'].length - 1)
      )
      console.log(rut)
      return rut
    },

    /** @param {HTMLDivElement} content */
    createModal(content) {
      const overlay = document.createElement('div')
      overlay.style.position = 'fixed'
      overlay.style.top = '0px'
      overlay.style.left = '0px'
      overlay.style.width = '100%'
      overlay.style.height = '100%'
      overlay.style.backgroundColor = 'rgba(0,0,0,.5)'
      overlay.style.zIndex = '50'
      overlay.classList.add('fadeIn')

      const modal = document.createElement('div')
      modal.style.width = '95%'
      modal.style.minHeight = '100px'
      modal.style.background = '#FFF'
      modal.style.zIndex = '100'
      modal.style.borderRadius = '4px'
      modal.style.display = 'flex'
      modal.style.justifyContent = 'center'
      modal.style.alignItems = 'center'
      modal.classList.add('fadeInUp')

      // const cerrar = document.createElement('img')
      // cerrar.src = CerrarSvg
      // cerrar.style.cursor = 'pointer'
      // cerrar.style.height = '24px'
      // cerrar.style.position = 'absolute'
      // cerrar.style.top = '10px'
      // cerrar.style.right = '10px'

      const modalContainer = document.createElement('div')
      modalContainer.style.width = '100%'
      modalContainer.style.height = '100%'
      modalContainer.style.position = 'fixed'
      modalContainer.style.top = '0px'
      modalContainer.style.left = '0px'
      modalContainer.style.borderRadius = '4px'
      modalContainer.style.display = 'flex'
      modalContainer.style.justifyContent = 'center'
      modalContainer.style.alignItems = 'center'
      modalContainer.style.zIndex = '100'

      cerrar.onclick = () => {
        modal.classList.add('fadeOut')
        modalContainer.classList.add('fadeOut')
        overlay.classList.add('fadeOut')
        setTimeout(() => {
          modal.remove()
          modalContainer.remove()
          overlay.remove()
        }, 500)
      }
      modal.append(cerrar)

      const contenido = document.createElement('div')
      contenido.innerHTML = content
      modal.append(contenido)
      modalContainer.append(modal)

      const element = document.querySelector('body')
      element.append(overlay)
      element.append(modalContainer)
    },

    /**
     * @descripcion Recibe el array entregado con el api de la cabecera de la grilla y lo transforma en un objeto para facilitar la interpolacion
     * @return {object}
     * @param {Array<object>} gridHeaders
     */
    procesGridHeaders(gridHeaders) {
      const columnObject = {}
      for (const element of gridHeaders) {
        columnObject[element['codigo']] = element['mensaje']
      }
      return columnObject
    },

    /** @return {string}
     * @param {number} T
     */
    digitoVerificador(T) {
      let M = 0
      let S = 1
      for (; T; T = Math.floor(T / 10)) {
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11
      }
      const digito = S - 1
      const dv = S ? digito + '' : 'k'
      return dv
    },

    downloadFile(response, fileName) {
      const fileNewName = fileName.replace(/\s+/g, '_');
      response.blob().then( async (b) => {
        const a = document.createElement('a')
        a.href = URL.createObjectURL(b)
        a.setAttribute('download', fileNewName)
        a.click()
      })
    },

  toast(text, delay, color) {
    color = color == undefined ? 'rgba(40,40,40,1)' : color
    const notification = document.createElement('div')
    notification.style.backgroundColor = color
    notification.style.color = '#FFFFFF'
    notification.style.padding = '15px'
    notification.style.position = 'fixed'
    notification.style.top = '20px'
    notification.style.right = '20px'
    notification.style.maxWidth = '300px'
    notification.style.borderRadius = '4px'
    notification.style.zIndex = '1000'
    notification.style.cursor = 'pointer'
    notification.innerHTML = text
    notification.onclick = () => {
      notification.classList.add('fadeOut')
      setTimeout(() => {
        notification.remove()
      }, 2000)
    }
    const body = document.querySelector('body')
    body.append(notification)
    setTimeout(() => {
      notification.classList.add('fadeOut')
      setTimeout(() => {
        notification.remove()
      }, 2000)
    }, delay)
  },

  ripple() {
    document.querySelectorAll('.ripple').forEach(ripple => {
      ripple.addEventListener('mousedown', e => {
    
        let posX = e.pageX - ripple.getBoundingClientRect().left;
        let posY = e.pageY - ripple.getBoundingClientRect().top;
        let buttonWidth = 1.5 * ripple.offsetWidth;
        let divRippleEffect = document.createElement('div');
        divRippleEffect.className = 'ripple-effect';
        divRippleEffect.style.width = `${buttonWidth}px`;
        divRippleEffect.style.height = `${buttonWidth}px`;
        divRippleEffect.style.left = `${ posX - (buttonWidth / 2) }px`;
        divRippleEffect.style.top = `${ posY - (buttonWidth / 2) }px`;
    
        ripple.appendChild(divRippleEffect);
    
        window.setTimeout(() => {
          ripple.removeChild(divRippleEffect);
        }, 2000);
      });
    });
  }
}
