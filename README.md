## ng-microfront-Utils

### Util 
monthNames: [
      'ENERO',
      'FEBRERO',
      'MARZO',
      'ABRIL',
      'MAYO',
      'JUNIO',
      'JULIO',
      'AGOSTO',
      'SEPTIEMBRE',
      'OCTUBRE',
      'NOVIEMBRE',
      'DICIEMBRE'
    ]

    /**
     * @description Abre un nuevo enlace, reemplaza el uso de window.location.href, funcionalidad que tiene conflictos con el analisis de kiuwan
     * @param {string} href
     */
    openInTab(href) 

    /**
     * @description Abre un nuevo enlace, reemplaza el uso de window.location.href, funcionalidad que tiene conflictos con el analisis de kiuwan
     * @param {string} href
     */
    openInNewTab(href)

    /**
     * @description Entrega rut con puntos y guión
     * @param {string} rut
     * @return {string} rutPuntos
     */
    formatRut(rut)

    /**
     * @description Funcion que permite evitar el paso de scripts en localstorage a modo de seguridad
     * @param {string} value
     * @return {string}
     */
    sanitizeValue(value)

    /**
     * @description Funcion que permite buscar el valor asociado al nombre de un parametro encontrado en el URL
     * @param {object} name
     * @return {string}
     */
    getParameterByName(name)

    /**
     * @description Esta funcion retorna los datos de usuario almacenados en local que son asignados cuando el usuario inicia sesión
     * @return {object}
     */
    getUserData()

    /**
     * @description
     * @return {object}
     */
    getMenu()

    /** @param {string} value
     * @return {string}
     */
    dateFormat(value)

    /** @param {number} input
     * @return {string}
     */
    generateDots(input)

    /**
     * @return {string}
     * @description retorna la una cadena que concatena la fecha actual, esta función es útil para generar el nombre de la descarga de archivos
     */
    getTimeStampString() 

    /** @return {number} */
    getCleanRutEmpresa() 

    /** @param {HTMLDivElement} content */
    createModal(content) 

    /**
     * @descripcion Recibe el array entregado con el api de la cabecera de la grilla y lo transforma en un objeto para facilitar la interpolacion
     * @return {object}
     * @param {Array<object>} gridHeaders
     */
    procesGridHeaders(gridHeaders)

  /** 
  @description retorna el dígito verificador
      @return {string}
    * @param {number} T
    */
  digitoVerificador(T)

  downloadFile(response, fileName) 

  toast(text: string, delay: number, color: string) 
